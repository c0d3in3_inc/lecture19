package com.c0d3in3.lecture19.register.ui

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import com.c0d3in3.lecture19.R
import com.c0d3in3.lecture19.auth.ui.AuthActivity
import com.c0d3in3.lecture19.network.`interface`.NetworkApiCallback
import com.c0d3in3.lecture19.network.api.NetworkApiHandler
import kotlinx.android.synthetic.main.activity_register.*
import java.util.*
import kotlin.properties.Delegates

class RegisterActivity : AppCompatActivity() {

    private lateinit var authTimer : Timer
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
    }

    override fun onStop() {
        //Handler().removeCallbacks { errorHandler }
        super.onStop()
    }

    private fun init() {
        registerButton.setOnClickListener {
            val email = emailEditText.text.toString()
            val password = passwordEditText.text.toString()
            val confirmPassword = confirmPasswordEditText.text.toString()
            if (email.isNotEmpty() && password.isNotEmpty() && confirmPassword.isNotEmpty()) {
                if (password == confirmPassword) register(email, password)
                else showError("Password's aren't equal!")
            } else showError("Please fill all the fields!")
        }
    }

    private fun showError(error : String) {
        messageTextView.setTextColor(Color.RED)
        messageTextView.text = "[ ! ] $error"
        if(messageTextView.visibility == View.GONE) messageTextView.visibility = View.VISIBLE
    }

        private fun authorize(token: String) {
        registerLayout.visibility = View.GONE
        loadingLayout.visibility = View.VISIBLE
        loadingTextView.setTextColor(Color.GREEN)
        var authTime = 5
        authTimer = Timer()
        authTimer.scheduleAtFixedRate(object : TimerTask() {
            override fun run() {
                runOnUiThread {
                    loadingTextView.text =
                        "Registration success!\nToken: $token\n\nYou will be redirected to Auth page in $authTime sec."
                }
                if (authTime == 0) {
                    authTimer.cancel()
                    val intent = Intent(applicationContext, AuthActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                    startActivity(intent)
                }
                else authTime--
            }

        }, 0, 1000)
    }

    private fun register(email: String, password: String) {
        val params = mutableMapOf<String, String>()
        params["email"] = email
        params["password"] = password
        NetworkApiHandler.postRequest(NetworkApiHandler.REGISTER, params, object : NetworkApiCallback {
            override fun onSuccess(response: String, code: Int) {
                if (code in 200..300) authorize(response)
                else showError(response)
            }
        })
    }
}
