package com.c0d3in3.lecture19.auth.ui

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Toast
import com.c0d3in3.lecture19.R
import com.c0d3in3.lecture19.network.`interface`.NetworkApiCallback
import com.c0d3in3.lecture19.network.api.NetworkApiHandler
import kotlinx.android.synthetic.main.activity_auth.*

class AuthActivity : AppCompatActivity() {

    private val errorHandler = Handler()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_auth)

        init()
    }

    private fun init(){
        authButton.setOnClickListener {
            val email = emailEditText.text.toString()
            val password = passwordEditText.text.toString()
            if (email.isNotEmpty() && password.isNotEmpty()) auth(email, password)
            else showError("Please fill all the fields!")
        }
    }

    private fun showError(text : String){
        messageTextView.setTextColor(Color.RED)
        messageTextView.text = "[ ! ] $text"
        if(messageTextView.visibility == View.GONE) messageTextView.visibility = View.VISIBLE
    }

    private fun authorize(token: String) {
        authLayout.visibility = View.GONE
        loadingLayout.visibility = View.VISIBLE
        loadingTextView.setTextColor(Color.GREEN)
        loadingTextView.text = "Auth success!\n\nYour token: $token"
    }

    private fun auth(email: String, password : String){
        val params = mutableMapOf<String, String>()
        params["email"] = email
        params["password"] = password
        NetworkApiHandler.postRequest(NetworkApiHandler.LOGIN, params, object : NetworkApiCallback{
            override fun onSuccess(response: String, code: Int) {
                if(code in 200..300){
                    authorize(response)
                }
                else{
                    showError(response)
                }
            }
        })
    }

}
