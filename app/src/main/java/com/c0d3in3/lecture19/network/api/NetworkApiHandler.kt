package com.c0d3in3.lecture19.network.api

import com.c0d3in3.lecture19.network.`interface`.NetworkApiCallback
import com.c0d3in3.lecture19.network.model.ResponseModel
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.*


object NetworkApiHandler {
    private const val BASE_URL = "https://reqres.in/api/"
    const val LOGIN = "login"
    const val REGISTER = "register"
    private var retrofit: Retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BASE_URL)
        .build()

    private var service: NetworkService = retrofit.create(NetworkService::class.java)

    interface NetworkService {
        @GET("{path}")
        fun getRequest(@Path("path") user: String): Call<String>

        @FormUrlEncoded
        @POST("{path}")
        fun postRequest(@Path("path") path: String,  @FieldMap params: MutableMap<String, String>): Call<String>
    }

    fun postRequest(path: String, params : MutableMap<String, String>, callback: NetworkApiCallback){
        val call = service.postRequest(path, params)
        call.enqueue(onCallback(callback))
    }

    fun getRequest(path : String, callback : NetworkApiCallback) {
        val call = service.getRequest(path)
        call.enqueue(onCallback(callback))
    }

    private fun onCallback(callback: NetworkApiCallback): Callback<String> =
        object : Callback<String>{
            override fun onFailure(call: Call<String>, t: Throwable) {
                callback.onFail(t.toString())
            }

            override fun onResponse(call: Call<String>, response: Response<String>) {
                if(response.isSuccessful){
                    val jsonObject = JSONObject(response.body())
                    callback.onSuccess(jsonObject["token"].toString(), response.code())
                }
                else{
                    val a = object : Annotation{}
                    val converter = retrofit.responseBodyConverter<ResponseModel>(
                        ResponseModel::class.java, arrayOf(a) )
                    val data = converter.convert(response.errorBody())
                    callback.onSuccess(data!!.error, response.code())
                }
            }

        }
}