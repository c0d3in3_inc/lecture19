package com.c0d3in3.lecture19.network.`interface`

interface NetworkApiCallback {
    fun onSuccess(response : String, code: Int){}
    fun onFail(response: String){}
}